# README #

Shiny App (1.5.3.838) running on AWS (Ubuntu 16.04.2 LTS) to explore Amazon transaction and review data from 1995 - 2005 with a focus on network analysis & prediction.
The app server will be shut down every night [online from 7am - 9pm, CET] keep cost at a minimum.

Authors:

* Fabian Fleischmann: ffleischmann8@gmail.com
* Daniel Rustenburg: rustenburgd@gmail.com 
* Mariusz Dudek: dudek.mariusz55@gmail.com 
* Jonathan von Rueden: jonathan.von.rueden@gmx.de


### Content ###

Data description:

* descriptive statistics for the Amazon data
* mainly plotly graphs (interactive) and data.tables

Network exploration:

* Overall statistics with centrality measures
* Co-purchasing analysis
* - search the entire data set (310877 products/nodes) and 1006538 connections (edges) among them
* - draw networks of sub-groups centered around selected product (e.g. try looking at Rich dad, poor dad - half a second needed to load when typing)
* - compare to another network from a second selected product
* - view title and centrality measures per product when hovering over
* - simulate connections between products and resulting network enlargement by adding nodes and edges on the fly (<edit>) button

Network analysis:

* Link prediction
* - select from all products and groups like in (2.)
* - determine degree, similarity threshold and similarity metric for network prediction
* - display newly predicted links and old links in graph
* Correlation Analysis
* - select any two correlation metrics and display correlation among them
* - overview heat-mapped correlation matrix

Data Processing

* The data Preparation is done in the "Data processing"-folder
* Processing is divided into different R files corresponding to the parts of the app the processed data is used for
* The different data frames, data tables and so on are saved after the processing in data.RData
* The data.RData file is then loaded before the execution of the shiny app and the calculated tables etc. can be used